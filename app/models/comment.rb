class Comment < ActiveRecord::Base
  belongs_to :user
  belongs_to :submission
  belongs_to :parent, class_name: "Comment"
  
  validates :message, length: { in: 5..1000 }
  
  has_many :replies, class_name: "Comment", foreign_key: "parent_id", :dependent => :destroy
  has_many :comment_votes, :dependent => :destroy
end
