class SubmissionVote < ActiveRecord::Base
  belongs_to :user
  belongs_to :submission
  
  validates :user_id, uniqueness: { scope: :submission_id }, presence: true
  validates :submission_id, presence: true
end
