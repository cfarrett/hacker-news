class User < ActiveRecord::Base
  has_many :submissions
  has_many :submission_votes
  has_many :comments
  has_many :comment_votes
  
  
  validates :name, presence: true
  validates :uid, uniqueness: true
  #validates :name, length: { in: 6..20 }
  #validates :name, :format => { /\A(?=.*[a-z])[a-z\d]+\Z/i } Rails format validation — alphanumeric, but not purely numeric (upper case allowed?)
  #validates :password, length: { in: 6..20 }
  
  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_initialize.tap do |user|
    #where(auth.slice(:provider, :uid)).first_or_initialize.tap do |user|
      user.provider = auth.provider
      user.uid = auth.uid
      user.name = auth.info.name
      user.picture = auth.info.image
      user.oauth_token = auth.credentials.token
      user.oauth_expires_at = Time.at(auth.credentials.expires_at)
      user.save!
    end
  end
end
