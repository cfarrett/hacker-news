class Submission < ActiveRecord::Base
  belongs_to :user, class_name: "User"
  
  validates :title, presence: true, uniqueness: false
  #validates :message, length: { maximum: 10000 }
  
  has_many :comments, class_name: "Comment", foreign_key: "submission_id", :dependent => :destroy
  has_many :submission_votes, :dependent => :destroy
  
  validate :urlXORmessage
  validates :url, :format => /(^$)|(^(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(([0-9]{1,5})?\/.*)?$)/ix

  private
    def urlXORmessage
      unless url.blank? ^ message.blank?
        errors.add(:base, "Submit a url or a message, not both")
      end
    end
end