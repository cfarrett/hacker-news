module SubmissionsHelper
  def currentUserHasUpVotedTheSubmission(submission)
    submission.submission_votes.each do |vote|
      if (vote.user_id == current_user.id)
        return true
      end
    end
    return false
  end
  
  def isCurrentUserSubmission(submission)
    if (submission.user_id == current_user.id)
      return true
    else return false
    end
  end
  
end
