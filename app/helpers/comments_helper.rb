module CommentsHelper
  def currentUserHasUpVotedTheComment(comment)
    comment.comment_votes.each do |vote|
      if (vote.user_id == current_user.id)
        return true
      end
    end
    return false
  end
  
  def isCurrentUserComment(comment)
    if (comment.user_id == current_user.id)
      return true
    else return false
    end
  end
  
end
