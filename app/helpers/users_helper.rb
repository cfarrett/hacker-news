module UsersHelper
  def hashedId(user)
    @x = Digest::SHA256.hexdigest user.uid
    return @x.force_encoding(Encoding::UTF_8)
  end
end