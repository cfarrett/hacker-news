class ApiUsersController < ApplicationController
  before_action :get_user, only: [:show, :update]
  before_action :get_all_users_formatted, only: [:index]

  # GET /users
  def index
    return renderOk(@formattedUsers)
  end
  
  # GET /users/1
  def show
    return renderOk(formatUser(@user))
  end
  
  # POST
  def create
    puts params.to_s
    if (params.has_key?(:name) && params.has_key?(:oauth_token) && params.has_key?(:uid) && params.has_key?(:picture) && params.has_key?(:oauth_expires_at))
      @user = User.find_by(uid: params[:uid])
      if (!@user.nil?)
        puts 'user exists'
        @user.update(name: params[:name], oauth_token: params[:oauth_token], uid: params[:uid], picture: params[:picture], oauth_expires_at: (params[:oauth_expires_at]))
        @user.update(user_params)
        return renderOk(formatFullUser(@user))
      else
        puts 'user does not exist'
        @user = User.new(user_params)
        @user.provider = "google_oauth2"
        @user.about = ""
        if (@user.save())
          puts 'user saved'
          return renderOk(formatFullUser(@user))
        else
          puts 'ERROR ERROR ERROR ERROR'
          return renderError('Bad Request', 400)
        end
      end
    else
      puts 'wrong params'
    end
  end

  # PATCH/PUT /users/1
  def update
    if (@user.id == @userId)
      if @user.update(user_params)
        return renderOk(formatUser(@user))
      else
        return renderError(find_error(@user.errors.messages), 400)
      end
    else
      return renderError('Unauthorized', 403)
    end
  end

  def get_user
    begin
      @user = User.find(params[:id])
    rescue
      return renderError('User not found')  
    end
  end
  
  
  # Never trust parameters from the scary internet, only allow the white list through.
  def user_params
    params.permit(:about, :name, :oauth_token, :uid, :picture, :oauth_expires_at)
  end
  
  
  def formatUser(user)
    tempUser = user.attributes.slice('id', 'name', 'picture', 'about', 'created_at');
    tempUser[:submissions] = user.submissions.size;
    tempUser[:comments] = user.comments.size;
    return tempUser
  end
  
  def formatFullUser(user)
    tempUser = user.attributes.slice('id', 'name', 'picture', 'about', 'created_at').merge('api-key' => hashedId(user));
    tempUser[:submissions] = user.submissions.size;
    tempUser[:comments] = user.comments.size;
    return tempUser
  end
  
  def get_all_users_formatted
    @formattedUsers = []
    users = User.all()
    users.each do |u|
      @formattedUsers.push(formatUser(u)) 
    end
  end
  
  def hashedId(user)
    @x = Digest::SHA256.hexdigest user.uid
    return @x.force_encoding(Encoding::UTF_8)
  end
end
