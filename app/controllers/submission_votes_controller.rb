class SubmissionVotesController < ApplicationController
  before_action :set_submission_vote, only: [:show, :edit, :update, :destroy]

  # GET /submission_votes
  # GET /submission_votes.json
  def index
    @submission_votes = SubmissionVote.all
  end

  # GET /submission_votes/1
  # GET /submission_votes/1.json
  def show
  end

  # GET /submission_votes/new
  def new
    @submission_vote = SubmissionVote.new
  end

  # GET /submission_votes/1/edit
  def edit
  end

  # POST /submission_votes
  # POST /submission_votes.json
  def create
    @submission_vote = SubmissionVote.new(submission_vote_params)

    respond_to do |format|
      if @submission_vote.save
        format.html { redirect_to @submission_vote, notice: 'Submission vote was successfully created.' }
        format.json { render :show, status: :created, location: @submission_vote }
      else
        format.html { render :new }
        format.json { render json: @submission_vote.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /submission_votes/1
  # PATCH/PUT /submission_votes/1.json
  def update
    respond_to do |format|
      if @submission_vote.update(submission_vote_params)
        format.html { redirect_to @submission_vote, notice: 'Submission vote was successfully updated.' }
        format.json { render :show, status: :ok, location: @submission_vote }
      else
        format.html { render :edit }
        format.json { render json: @submission_vote.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /submission_votes/1
  # DELETE /submission_votes/1.json
  def destroy
    @submission_vote.destroy
    respond_to do |format|
      format.html { redirect_to submission_votes_url, notice: 'Submission vote was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_submission_vote
      @submission_vote = SubmissionVote.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def submission_vote_params
      params.require(:submission_vote).permit(:user_id, :submission_id, :value)
    end
end
