class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  
  #TODO UNCOMMENT THIS FOR PRODUCTION
  #protect_from_forgery with: :exception
  protect_from_forgery
  helper_method :current_user
  before_filter :check_user 


  # Use callbacks to share common setup or constraints between actions.
    def check_user

        if(request.path.downcase().include? "/api/" )
            unless (request.get? and !request.path.downcase().include? "threads") or (request.post? and request.path.downcase().include? "users")
                @userId = request.headers["id"]
                @userApiKey = request.headers["api-key"]
    
                if(@userId.nil?)
                    return renderError("Must provide id", 401)
                else
                    @userId = @userId.to_i
                end
                
                if(@userApiKey.nil?)
                    return renderError("Must provide apikey", 401)
                end
        
                begin
                    @reqUser = User.find(@userId)   
                rescue ActiveRecord::RecordNotFound
                    return renderError("The id provided doesn't match any user in the system", 401) 
                end
                
                @x = Digest::SHA256.hexdigest @reqUser.uid
                @x = @x.force_encoding(Encoding::UTF_8)
                  
                if(@x != @userApiKey)
                    return renderError('Your id or api-key are incorrect', 401)
                end
            end
        end
    end

    
    def current_user
        @current_user ||= User.find(session[:user_id]) if session[:user_id]
    end

    def formatResponse(content)
      @toJson = content.as_json
      if(@toJson.is_a?(Array))
        return @toJson
      elsif(@toJson.is_a?(String))
        return Array.new([@toJson])  
      else
        return @toJson
      end
    end

    def find_error(errorHash)
        errorHash.each do |key,value|
          return key.to_s + ': ' + value[0].to_s
        end
    end

    def renderError(error,stat = 404)
      render json: {message: error, status: stat}, status: stat 
    end
    
    def renderOk(data, stat = 200)
      @dataFormatted = formatResponse(data)
      puts @dataFormatted.to_s
      render json: @dataFormatted, status: stat
    end

end