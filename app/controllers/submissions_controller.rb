class SubmissionsController < ApplicationController
  before_action :set_submission, only: [:show, :edit, :update, :destroy]

  # GET /submissions
  # GET /submissions.json
  def index
    @submissions = Submission.order(created_at: :desc)
  end

  # GET /submissions/1
  # GET /submissions/1.json
  def show
    @comment = Comment.new
    
    if (current_user) 
      @comment.user_id = current_user.id
    else 
      @comment.user_id = nil
    end
    
    @comment.submission_id = @submission.id
  
    @comments = Comment.where("submission_id = ? AND parent_id IS NULL", params[:id]).order(created_at: :asc)
    @replies = Comment.where("submission_id = ? AND parent_id IS NOT NULL", params[:id]).order(parent_id: :asc ,created_at: :asc)
  end

  # GET /submissions/new
  def new
    @submission = Submission.new
    @submission.user_id = current_user.id
  end

  # POST /submissions
  # POST /submissions.json
  def create
    @submission = Submission.new(submission_params)
    
    respond_to do |format|
      if @submission.save
        format.html { redirect_to @submission, notice: 'Submission was successfully created.' }
        format.json { render :show, status: :created, location: @submission }
      else
        format.html { render :new }
        format.json { render json: @submission.errors, status: :unprocessable_entity }
      end
    end
  end

  def upvote
    @userId = current_user.id
    @submissionId = params[:id]
    
    @vote = SubmissionVote.find_by(user_id: @userId, submission_id: @submissionId)
    
    if (@vote.nil?) 
      SubmissionVote.create(user_id: @userId, submission_id: @submissionId, value: 1)
    else 
      @vote.destroy
    end
    redirect_to(:back)  
  end
  
  def ask
    @submissions = Submission.where("url = ''").order(created_at: :desc)
  end
  
  def news
    @submissions = Submission.where("message = ''").order(created_at: :desc)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_submission
      @submission = Submission.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def submission_params
      params.require(:submission).permit(:user_id, :title, :url, :message)
    end
end
