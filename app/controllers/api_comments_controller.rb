class ApiCommentsController < ApplicationController
  before_action :get_comment, only: [:show, :vote, :reply]

  # GET /comments
  # GET /comments.json
  def index
    @res = [];
    @comments = Comment.order(created_at: :desc)
    @comments.each do |c|
      @res.push(format_comment(c)) 
    end
    return renderOk(@res)
  end

  # GET /comments/1
  # GET /comments/1.json
  def show
    return renderOk(format_comment_with_replies(@comment))
  end
  
  
  # POST /comments/1/reply
  def reply 
    @newComment = Comment.new(comment_params)
    @newComment.user_id = @userId
    @newComment.parent_id = params[:id]
    
    if !@comment.parent_id.nil?
      return renderError('You cannot reply to a reply comment')
    end
    
    @newComment.submission_id = @comment.submission_id
    if @newComment.save
      return renderOk(format_comment(@newComment))
    else
      return renderError(find_error(@newComment.errors.messages), 400)
    end
  end
  
  # POST /comments/1/vote
  def vote
    
    if @comment.user_id == @userId
      return renderError('You cannot vote your own comment')
    end
    
    @vote = CommentVote.find_by(user_id: @userId, comment_id: @comment.id)
    
    if (@vote.nil?) 
      CommentVote.create(user_id: @userId, comment_id: @comment.id, value: 1)
      
      return renderOk('Voted Comment')
    else 
      @vote.destroy
      return renderOk('Deleted Vote')
    end
  end
  
  # GET /threads
  def threads
    @res = [];
    @comments = Comment.where(user_id: @userId).order(created_at: :desc)
    @comments.each do |c|
      @res.push(format_comment(c)) 
    end
    return renderOk(@res)
  end
  
  private
    def get_comment
      begin
        @comment = Comment.find(params[:id])
      rescue
        return renderError('Comment not found')  
      end
    end
    
    def format_comment(comment)
      tmpUser = comment.attributes.slice('id', 'user_id', 'submission_id', 'parent_id', 'message','created_at')
                  .merge("user_name" => User.find(comment.user_id).name)
                  .merge("points" => comment.comment_votes.size + 1)
                  .merge('_links' => get_links(comment,'/api/v1/comments/'))
                  
      if (request.headers["id"])
        puts 'header', request.headers["id"]
        puts 'submission', comment.id
        vote = CommentVote.where("user_id = ? AND comment_id = ?", request.headers["id"], comment.id)
        if (vote.size>0)
          tmpUser = tmpUser.merge("hasBeenUpvoted" => vote.first.attributes['value']);
        end
      end
      
      return tmpUser
    end
    
    def format_comment_with_replies(comment)
      @replies = Comment.where("parent_id = ?", comment.id).order(created_at: :asc)
      @resReplies = []
      @replies.each do |r|
        reply = r.attributes.slice('id', 'user_id', 'message', 'created_at')
          .merge("points" => r.comment_votes.size + 1)
          .merge("user_name" => User.find(r.user_id).name)
          
        if (request.headers["id"])
          vote = CommentVote.where("user_id = ? AND comment_id = ?", request.headers["id"], r.id)
          if (vote.size>0)
            reply = reply.merge("hasBeenUpvoted" => vote.first.attributes['value']);
          end
        end
        @resReplies.push(reply)
      end
      
      tmpUser = comment.attributes.slice('id', 'user_id', 'submission_id', 'parent_id', 'message','created_at')
                  .merge("user_name" => User.find(comment.user_id).name)
                  .merge("points" => comment.comment_votes.size + 1)
                  .merge('_links' => get_links(comment,'/api/v1/comments/'))
                  .merge("replies" => @resReplies)
                  
      if (request.headers["id"])
        puts 'header', request.headers["id"]
        puts 'submission', comment.id
        vote = CommentVote.where("user_id = ? AND comment_id = ?", request.headers["id"], comment.id)
        if (vote.size>0)
          tmpUser = tmpUser.merge("hasBeenUpvoted" => vote.first.attributes['value']);
        end
      end
                  
      return tmpUser
    end
    
    # Never trust parameters from the scary internet, only allow the white list through.
    def comment_params
      params.permit(:submission_id, :message)
    end
    
    def get_links(comment, path)
      submissionsPath = '/api/v1/submissions/'
      commentsPath = '/api/v1/comments/'
      parentPath = comment.parent_id.nil? ? nil : commentsPath + comment.parent_id.to_s
      return {
          'self' => {'href' => path + comment.id.to_s}, 
          'submission' => {'href' => submissionsPath + comment.submission_id.to_s  },
          'parent' => {'href' =>  parentPath},
      }
    end
end
