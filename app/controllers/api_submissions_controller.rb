class ApiSubmissionsController < ApplicationController
  before_action :get_submission, only: [:show, :vote, :reply]

  # GET /submissions
  def index
    @resAsks = [];
    @resNews = [];

    @news = Submission.where("message = ''").order(created_at: :desc)
    @asks = Submission.where("url = ''").order(created_at: :desc)
    
    @news.each do |n|
      @resNews.push(formatSubmission(n, 'news'))
    end
    
    @asks.each do |a|
      @resAsks.push(formatSubmission(a,'asks'))
    end
    
    @jsonResult = { 'asks' => @resAsks , 'news' => @resNews}
    return renderOk(@jsonResult)
  end
  

  # GET /submissions/1
  def show
    @comments = Comment.where("submission_id = ? AND parent_id IS NULL", @submission.id).order(created_at: :asc)

    @resComments = []
    
    @comments.each do |c|
      @replies = Comment.where("submission_id = ? AND parent_id = ?", @submission.id, c.id).order(parent_id: :asc ,created_at: :asc)
      @resReplies = []
      @replies.each do |r|
        reply = r.attributes.slice('id', 'user_id', 'message', 'created_at')
          .merge("points" => r.comment_votes.size + 1)
          .merge("user_name" => User.find(r.user_id).name)
        
        if (request.headers["id"])
          vote = CommentVote.where("user_id = ? AND comment_id = ?", request.headers["id"], r.id)
          if (vote.size>0)
            reply = reply.merge("hasBeenUpvoted" => vote.first.attributes['value']);
          end
        end
        @resReplies.push(reply)
          
      puts 'replies', @resReplies.size    
      end
      
      comment = c.attributes.slice('id', 'user_id', 'message', 'created_at')
        .merge("points" => c.comment_votes.size + 1)
        .merge("user_name" => User.find(c.user_id).name)
        .merge("replies" => @resReplies)
        
      if (request.headers["id"])
        vote = CommentVote.where("user_id = ? AND comment_id = ?", request.headers["id"], c.id)
        if (vote.size>0)
          comment = comment.merge("hasBeenUpvoted" => vote.first.attributes['value']);
        end
      end
      @resComments.push(comment)
    end
    
    type = @submission.message.blank? ? 'news' : 'asks'
    return renderOk(formatSubmission(@submission, type, true))
  end

  # POST /news or /asks
  def create
    @submission = Submission.new(submission_params)
    @submission.user_id = @userId
    
    if (request.path.downcase().include? 'news')
      if (@submission.url.nil?) 
        return renderError('An url must be provided', 400)
      elsif(@submission.message.nil?)
        @submission.message=''
        type = 'news'
      end
    end

    if (request.path.downcase().include? 'asks')
      if (@submission.message.nil?) 
        return renderError('A message must be provided', 400)
      elsif(@submission.url.nil?)
        @submission.url=''
        type = 'asks'
      end
    end

    if @submission.save
      renderOk(formatSubmission(@submission,type))
    else
      renderError(find_error(@submission.errors.messages), 400)
    end
  end
  
  # POST /submissions/1/reply
  def reply
    @comment = Comment.new(comment_params)
    @comment.user_id = @userId
    @comment.submission_id = @submission.id
    if @comment.save
      renderOk(format_comment(@comment))
    else
      return renderError(find_error(@comment.errors.messages), 400)
    end
  end

  # POST /submissions/1/vote
  def vote
    
    if @submission.user_id == @userId
      return renderError('You cannot vote your own submission')
    end

    @vote = SubmissionVote.find_by(user_id: @userId, submission_id: @submission.id)
    
    if (@vote.nil?)
      SubmissionVote.create(user_id: @userId, submission_id: @submission.id, value: 1)
      renderOk('Voted submission')
    else 
      @vote.destroy
      renderOk('Deleted Vote')
    end
  end
  
  # GET /asks
  def asks
    @res = []
    @submissions = Submission.where("url = ''").order(created_at: :desc)
    
    @submissions.each do |s|
      @res.push(formatSubmission(s, 'asks'))
    end
    renderOk(@res)
  end
  
  # GET /news
  def news
    @res = []
    @submissions = Submission.where("message = ''").order(created_at: :desc)
    
    @submissions.each do |s|
      @res.push(formatSubmission(s, 'news'))
    end
    renderOk(@res)
  end

  private
    def get_submission
      begin
        @submission = Submission.find(params[:id])
      rescue ActiveRecord::RecordNotFound
        return renderError('Submission not found')  
      end
    end
    
    def formatSubmission(submission, type, showComments = false)  
      url = type.include? 'news'
      
      puts 'submission id ', submission.id
      
      formatted = 
        url ? submission.attributes.slice('id', 'user_id', 'title', 'url' , 'created_at') 
        : submission.attributes.slice('id', 'user_id', 'title', 'message' , 'created_at')
      formatted = formatted.merge("points" => submission.submission_votes.size + 1)
        .merge("user_name" => User.find(submission.user_id).name)
        .merge("comments" => submission.comments.size)
        .merge('_links' => get_links(submission, '/api/v1/submissions/'))
        
      

      if (request.headers["id"])
        puts 'header', request.headers["id"]
        puts 'submission', submission.id
        vote = SubmissionVote.where("user_id = ? AND submission_id = ?", request.headers["id"], submission.id)
        if (vote.size>0)
          formatted = formatted.merge("hasBeenUpvoted" => vote.first.attributes['value']);
        end
        
      end
        
      if(showComments) 
        formatted = formatted.merge("comments" => @resComments)
      end

      return formatted
    end
    
    def format_comment(comment)
      tmpUser = comment.attributes.slice('id', 'user_id', 'submission_id', 'parent_id', 'message','created_at')
                  .merge("user_name" => User.find(comment.user_id).name)
                  .merge("points" => comment.comment_votes.size + 1)
                  .merge('_links' => get_links(comment,'/api/v1/comments/'))
                  
      if (request.headers["id"])
        puts 'header', request.headers["id"]
        puts 'submission', comment.id
        vote = CommentVote.where("user_id = ? AND comment_id = ?", request.headers["id"], comment.id)
        if (vote.size>0)
          tmpUser = tmpUser.merge("hasBeenUpvoted" => vote.first.attributes['value']);
        end
      end
      
      return tmpUser
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def submission_params
      params.permit(:title, :url, :message)
    end
    
    def comment_params
      params.permit(:message)
    end
    
    def get_links(submission, path)
      usersPath = '/api/v1/users/'
      
      {
          'self' => {'href' => path + submission.id.to_s}, 
          'author' => {'href' => usersPath + submission.user_id.to_s}
      }
    end
  end
