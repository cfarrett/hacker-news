class CommentsController < ApplicationController
  before_action :set_comment, only: [:show, :edit, :update, :destroy]

  # GET /comments
  # GET /comments.json
  def index
    @comments = Comment.order(created_at: :desc)
    
  end

  # GET /comments/1
  # GET /comments/1.json
  def show
    
    @reply = Comment.new
    if (current_user) 
      @reply.user_id = current_user.id
    else 
      @reply.user_id = nil
    end
  end

  # POST /comments
  # POST /comments.json
  def create
    @comment = Comment.new(comment_params)

    respond_to do |format|
      if @comment.save
        @submission = Submission.find(@comment.submission_id)
        
        if (@comment.parent_id.nil?)
          format.html { redirect_to @submission, notice: 'Comment was successfully created.' }
        else 
          format.html { redirect_to @submission, notice: 'Reply was successfully created.' }
        end
        
        format.json { render :show, status: :created, location: @comment }
      else
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  
  def upvote
    @userId = current_user.id
    @commentId = params[:id]
    
    @vote = CommentVote.find_by(user_id: @userId, comment_id: @commentId)
    
    if (@vote.nil?) 
      CommentVote.create(user_id: @userId, comment_id: @commentId, value: 1)
    else 
      @vote.destroy
    end
    redirect_to(:back)  
  end
  
  
  def threads
    @userId = current_user.id
    @comments = Comment.where(user_id: @userId).order(created_at: :desc)
    
  end
  
  


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_comment
      @comment = Comment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def comment_params
      params.require(:comment).permit(:user_id, :submission_id, :parent_id, :message)
    end
end
