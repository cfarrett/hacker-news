json.array!(@comments) do |comment|
  json.extract! comment, :id, :user_id, :submission_id, :parent_id, :message
  json.url comment_url(comment, format: :json)
end
