json.array!(@submissions) do |submission|
  json.extract! submission, :id, :user_id, :title, :url
  json.url submission_url(submission, format: :json)
end
