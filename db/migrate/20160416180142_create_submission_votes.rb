class CreateSubmissionVotes < ActiveRecord::Migration
  def change
    create_table :submission_votes do |t|
      t.references :user, index: true, foreign_key: true
      t.references :submission, index: true, foreign_key: true
      t.string :value

      t.timestamps null: false
    end
  end
end
