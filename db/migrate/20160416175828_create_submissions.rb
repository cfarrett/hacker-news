class CreateSubmissions < ActiveRecord::Migration
  def change
    create_table :submissions do |t|
      t.references :user, index: true, foreign_key: true
      t.string :title
      t.text :message
      t.string :url

      t.timestamps null: false
    end
  end
end
