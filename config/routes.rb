Rails.application.routes.draw do

  root to: "submissions#index"

  get 'sessions/create'

  get 'sessions/destroy'

  get 'auth/:provider/callback', to: 'sessions#create'
  get 'auth/failure', to: redirect('/')
  get 'signout', to: 'sessions#destroy', as: 'signout'

  resources :sessions, only: [:create, :destroy]

  resources :comment_votes

  resources :comments do
    member do
      get 'upvote' => 'comments#upvote'
    end
  end  
  
  resources :submissions do
    member do
      get 'upvote' => 'submissions#upvote'
    end
  end  
  

  resources :submission_votes
  resources :submissions
  resources :votes
  resources :contributions
  resources :users
  
  get '/asks', to: 'submissions#ask'
  get '/news', to: 'submissions#news'
  
  get '/threads', to: 'comments#threads'


  scope '/api' do
    scope '/v1' do
      
      
      scope '/users' do
        get '/' => 'api_users#index'
        post '/'=> 'api_users#create'
        scope '/:id' do
          get '/' => 'api_users#show'
          put '/' => 'api_users#update'
          patch '/' => 'api_users#update'
        end
      end
      
      scope '/submissions' do
        get '/' => 'api_submissions#index'
        #post '/' => 'api_submissions#create'
        scope '/:id' do
          get '/' => 'api_submissions#show'
          post '/reply' => 'api_submissions#reply'
          scope '/vote' do
            post '/' => 'api_submissions#vote'
          end
          
        end
      end
      
      scope '/comments' do
        get '/' => 'api_comments#index'
        scope '/:id' do
          get '/' => 'api_comments#show'
          post '/reply' => 'api_comments#reply'
          scope '/vote' do
            post '/' => 'api_comments#vote'
          end
        end
      end 
      
      scope '/asks' do
        get '/' => 'api_submissions#asks'
        post '/' => 'api_submissions#create'
      end 

      scope '/news' do
        get '/' => 'api_submissions#news'
        post '/' => 'api_submissions#create'
      end 
    
      scope '/threads' do
        get '/' => 'api_comments#threads'
      end
    end
  end
end
