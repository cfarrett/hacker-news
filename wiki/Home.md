# Welcome

Benvigunt a la wiki!

#HORES TOTALS:
##SEGONA ENTREGA:
  - 30 hores 

##Estructura dels models:

###User:

####Create
```
rails generate scaffold User name:string provider:string oauth_token:string uid:integer picture:text oauth_expires_at:datetime about:string
```
####Destroy
```
rails destroy scaffold scaffold User name:string provider:string oauth_token:string uid:integer picture:text oauth_expires_at:datetime about:string
```


###Submission:
####Create
```
rails generate scaffold Submission title:string message:text url:string user:references
```
####Destroy
```
rails destroy scaffold Submission title:string message:text url:string user:references
```


###SubmissionVote:
#Create
```
rails generate scaffold SubmissionVote user:references submission:references value:string

```
####Destroy
```
rails destroy scaffold SubmissionVote user:references submission:references value:string
```


###Comment:
####Create
```
rails generate scaffold Comment user:references submission:references parent:references message:text
```
####Destroy
```
rails destroy scaffold Comment title:string message:text url:string user:references
```


###CommentVote:
#Create
```
rails generate scaffold CommentVote user:references comment:references value:string

```
####Destroy
```
rails destroy scaffold CommentVote user:references comment:references value:string
```

##Eliminar BDS:
```
bundle exec rake db:rollback
```

##Actualitzar BDS:
```
bundle exec rake db:migrate
```


##ISSUES


###Alternatives a SQLite
El primer debat va sorgir a l'hora de decidir si utilitzavem SQLite o bé, es buscava una altra alternativa més potent, com pot ser MongoDB.
A JavaScript, existeix el framework Mongoose, que afegeix moltes funcionalitats útils, com elles la opció de "popular" els documents, cosa que vist 
el domini del projecte, ens estalviava molta feina.

Després d'investigar, la única opció que implementava "populate", era antiga i estava deprecada, així que al final, hem decidit utilitzar SQLite.


###Date time
SQLite (l'entorn local) no té date time, així que per les dates seran guardades com a ISOString i les parsejarem quan sigui necessari.


###Migrate de taules que ja estan creades:
[SO Solution](http://stackoverflow.com/questions/7874330/rake-aborted-table-users-already-exists)

###Afegir un atribut a una taula ja creada:


####Add attribute to existing models (Not working)
```
rails generate migration AddColumnNameToUser numOfComments:integer numOfSubmissions:integer
rake db:migrate
```
###Reset DB
```
rake db:drop && rake db:create && rake db:migrate && rake db:seed
```
###Recuperar la informació al executar el callbakc de l'oAuth:
  Strong variables

###Deploy a Heroku incorrecte 
##Twitter bootstrap
Twitter bootstrap crea certs arxius, el problema ha estat que al esborrar la gem no s'han eliminat totes les dependències.
Hi ha una dependencia que no trobem, que requireix l'arxiu en.bootstrap.yml tot i que no sigui util. Si no està, dona un error 
i no podem deployar a heroku.

##Multiline comments en HTML
Fer comentaris multilinia en HTML usant RoR pot donar problemes al parser de Heroku.

##Problema amb la db
Al fer un deploy nou hi ha conflicte amb la bd de postgres al fer el migrate. 
La solució és utilitzar les següents comandes:
 - heroku pg:reset DATABASE_URL
 - heroku run rake db:migrate

##CORS Problema
Problema al fer el request de oAuth a Heroku, solucionat afegint a /config/application.rb les següents linies:
```
config.action_dispatch.default_headers.merge!({
  'Access-Control-Allow-Origin' => '*',
  'Access-Control-Request-Method' => '*'
})
´´´


##Problema ActionController::InvalidAuthenticityToken 
```
protect_from_forgery with: :exception
```
s'ha de canviar a 

```
protect_from_forgery
```

El protect_from_forgery s'encarrega de prevenir atacs CSRF (Cross site request forgery). CSRF es basa en que la web accepta qualsevol comanda d'un usuari conegut.


##Problema amb CORS i SWAGGER
Tot i haver autoritzat varis headers anteriorment, no són suficients. Per això hem utiltizat la gema
[Rack-cors](https://github.com/cyu/rack-cors) que permet configurar de manera senzilla les restriccions CORS.


##Problema per a definir tipus de respostes diferents per a a la mateixa ruta
Si una petició retorna diferents tipus d'objectes, tenim el problema de que swagger no ho permet
ja que es basa en que les APIs han de ser deterministiques.
Això implica estrucutrar les apis de certa manera normalment dividint en diferents rutes.
Per això hem definit dos GETS, un per a news i un altre per a asks.
