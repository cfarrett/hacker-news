#HNROR

Readme del projecte Hacker News desenvolupat en RoR per l'assignatura ASW.

Fet per:

  - Daniel Domínguez
  - Cristian Jara
  - Marçal Peiró
  - Marcos Pérez


#Executar el projecte de manera local:
No redactat.

#Executar el projecte en c9.io:
  - git pull (per si de cas)
  - gem install bundle
  - bundle install
  - bundle update
  - Fer click a run project
  - (si tens problemes, fer *$ rails s -b $IP -p $PORT*)

#Deploy de producció a Heroku:
  - Afegir al vostre usuari local el remote de heroku:
    - git remote add heroku https://git.heroku.com/hacker-news-asw-dcmm.git
  - Fer login a heroku: 
    - heroku login
    - heroku keys:add
  - git push heroku master
  - heroku run rake db:migrate


#Convencions, guíes i altres enllaços:

  - [Rails style guide](https://github.com/bbatsov/rails-style-guide)
  - [Bitbucket repo](https://bitbucket.org/cfarrett/hacker-news)
  - [Llibre de l'assignatura](https://www.railstutorial.org/book/beginning#sec-mvc)
  - [Heroku link](https://hacker-news-asw-dcmm.herokuapp.com/)